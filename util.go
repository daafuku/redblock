package redblock

import "math/rand"

func generateName() string {
	s := make([]rune, 8)
	for i := 0; i < 8; i++ {
		s[i] = rand.Int31n(26) + 'a'
	}
	return string(s)
}
