package redblock

type Copyable interface {
	Copy() interface{}
}

func copy(a interface{}) (b interface{}) {
	switch a := a.(type) {
	case Copyable:
		b = a.Copy()
	default:
		b = a
	}
	return
}
