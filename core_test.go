package redblock

import (
	"context"
	"log"
	"strings"
	"testing"
)

func TestBlockInIsolation(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())

	upper := NewBlock(
		func(message Message, block *Block) {
			s := message.(string)
			block.Send(0, strings.ToUpper(s))
		},
		NumOutputs(1),
	)

	ch := make(chan struct{})

	printer := NewBlock(func(message Message, block *Block) {
		log.Println(message)
		ch <- struct{}{}
	})

	upper.outputs = [][]*Block{{printer}}

	go upper.run(ctx)
	go printer.run(ctx)

	count := 10

	for i := 0; i < count; i++ {
		upper.Inject("test")
		log.Println(len(upper.queue))
	}

	for i := 0; i < count; i++ {
		<-ch
	}

	cancel()
}

func TestRecursive(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())

	ch := make(chan struct{})

	block := NewBlock(func(message Message, block *Block) {
		i := message.(int)
		if i == 0 {
			ch <- struct{}{}
			return
		}
		log.Println(i)
		block.Send(0, i-1)
	},
		NumOutputs(1))

	block.outputs = [][]*Block{{block}}

	go block.run(ctx)

	block.messages <- 10

	<-ch

	cancel()
}
