package redblock

import (
	"context"
	"log"
	"sync"
	"testing"
)

func createPrintAndForward(ch chan struct{}) Fn {
	return func(m Message, b *Block) {
		log.Println(m)
		b.Send(0, m)
		ch <- struct{}{}
	}
}

func TestFlow(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())

	flow := NewFlow()

	ch := make(chan struct{})

	first := flow.AddBlock(NewBlock(createPrintAndForward(ch), NumOutputs(1)))
	second := flow.AddBlock(NewBlock(createPrintAndForward(ch), NumOutputs(1)))

	flow.Connect(second, first, 0)
	flow.Start(ctx)
	flow.Inject(first, "test")

	<-ch
	<-ch

	cancel()
}

func TestFlowWithCopy(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	flow := NewFlow()

	agg := []Message{}

	wg := &sync.WaitGroup{}

	first := flow.AddBlock(NewBlock(
		func(message Message, block *Block) { block.Send(0, message) },
		NumOutputs(1),
		WaitGroup(wg),
	))

	second := flow.AddBlock(NewBlock(func(message Message, block *Block) {
		agg = append(agg, message)
	}, WaitGroup(wg)))

	third := flow.AddBlock(NewBlock(func(message Message, block *Block) {
		agg = append(agg, message.(int)*2)
	}, WaitGroup(wg)))

	flow.Connect(second, first, 0)
	flow.Connect(third, first, 0)

	flow.Start(ctx)

	flow.Inject(first, 1)

	wg.Wait()

	if len(agg) != 2 || agg[0] != 1 || agg[1] != 2 {
		t.Error("cloned values did not propagate")
	}
}
