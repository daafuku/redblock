package redblock

import "sync"

type BlockOption func(block *Block) error

const (
	BlockAsyncInfinite = -1
)

func Async(async int) BlockOption {
	return BlockOption(func(block *Block) error {
		if block.async != 0 {
			panic("duplicate Async BlockOption")
		}
		block.async = async
		return nil
	})
}

func WaitGroup(wg *sync.WaitGroup) BlockOption {
	return BlockOption(func(block *Block) error {
		if block.wg != nil {
			panic("duplicate WaitGroup BlockOption")
		}
		block.wg = wg
		return nil
	})
}

func NumOutputs(ports int) BlockOption {
	return BlockOption(func(block *Block) error {
		if block.outputs != nil {
			panic("duplicate Ports BlockOption")
		}
		block.outputs = make([][]*Block, ports)
		return nil
	})
}

func BlockName(name string) BlockOption {
	return BlockOption(func(block *Block) error {
		block.name = name
		return nil
	})
}
