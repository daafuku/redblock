package redblock

import (
	"context"
)

type Flow struct {
	blocks map[string]*Block
}

func NewFlow() (flow *Flow) {
	flow = &Flow{
		blocks: make(map[string]*Block),
	}
	return
}

func (flow *Flow) AddBlock(block *Block) string {
	name := block.name
	flow.blocks[name] = block
	return name
}

func (flow *Flow) Connect(dst, src string, port int) {
	if d, de := flow.blocks[dst]; de {
		if s, se := flow.blocks[src]; se {
			if s.outputs[port] == nil {
				s.outputs[port] = []*Block{}
			}
			s.outputs[port] = append(s.outputs[port], d)
		}
	}
}

func (flow *Flow) Start(ctx context.Context) {
	for _, block := range flow.blocks {
		go block.run(ctx)
	}
}

func (flow *Flow) Inject(dst string, message Message) {
	if d, de := flow.blocks[dst]; de {
		d.Inject(message)
	}
}
