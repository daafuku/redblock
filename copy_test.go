package redblock

import (
	"context"
	"sync"
	"testing"
)

type copyTest struct {
	copied bool
}

func (c *copyTest) Copy() interface{} {
	message := &copyTest{
		copied: true,
	}

	return message
}
func TestCopy(t *testing.T) {
	m := []Message{}

	wg := &sync.WaitGroup{}

	c := NewBlock(func(message Message, block *Block) {
		m = append(m, message)
	}, WaitGroup(wg))

	b := NewBlock(func(message Message, block *Block) {
		block.Send(0, message)
	}, NumOutputs(1), WaitGroup(wg))

	b.outputs[0] = []*Block{c, c}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	go b.run(ctx)
	go c.run(ctx)

	b.Inject(&copyTest{
		copied: false,
	})

	wg.Wait()

	if len(m) != 2 {
		t.Error("bad length for messages")
		t.FailNow()
	}

	if c, o := m[0].(*copyTest); !o || !c.copied {
		t.Error("struct wasn't copied")
	}
	if c, o := m[1].(*copyTest); !o || !c.copied {
		t.Error("struct wasn't copied")
	}
}
