package redblock

import (
	"context"
	"sync"
)

type Message interface{}

type Fn func(message Message, block *Block)

type Block struct {
	name       string
	load       int
	async      int
	messages   chan Message
	completion chan struct{}
	queue      []Message
	fn         Fn
	outputs    [][]*Block
	wg         *sync.WaitGroup
}

func NewBlock(fn Fn, options ...BlockOption) *Block {
	block := new(Block)

	block.fn = fn

	for _, option := range options {
		option(block)
	}

	if block.name == "" {
		block.name = generateName()
	}

	if block.async <= 0 && block.async != BlockAsyncInfinite {
		block.async = 1
	}

	block.messages = make(chan Message)
	block.completion = make(chan struct{})

	return block
}

func (block *Block) Send(port int, message Message) {
	if len(block.outputs) <= port {
		// discard message
		return
	}

	if block.outputs[port] == nil || len(block.outputs[port]) < 1 {
		// discard message
		return
	}

	if len(block.outputs[port]) > 1 {
		// multiple outputs connected
		for _, output := range block.outputs[port] {
			m := copy(message)
			output.Inject(m)
		}
		return
	}

	block.outputs[port][0].Inject(message)
}

func (block *Block) Inject(message Message) {
	if block.wg != nil {
		block.wg.Add(1)
	}
	block.messages <- message
}

func (block *Block) run(ctx context.Context) {
	for {
		select {
		case <-ctx.Done():
			return
		case message := <-block.messages:
			if block.async == BlockAsyncInfinite || block.load < block.async {
				go block.act(message)
			} else {
				block.queue = append(block.queue, message)
			}
			block.load++
		case <-block.completion:
			block.load--
			if block.wg != nil {
				block.wg.Add(-1)
			}
			if len(block.queue) > 0 {
				message := block.queue[0]
				block.queue = block.queue[1:]
				go block.act(message)
			}
		}
	}
}

func (block *Block) act(message Message) {
	block.fn(message, block)
	block.completion <- struct{}{}
}
